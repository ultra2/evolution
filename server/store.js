import {
  MongoClient
} from 'mongodb'
import {
  Utils
} from './util'

export class Store {
  constructor() {
    // this.url = "mongodb://admin:TV123456@ds040637.mlab.com:40637/evolution"
    this.url = 'mongodb://localhost:27017/evolution'
  }

  async initMongo() {
    try {
      this.db = await MongoClient.connect(this.url, {
        autoReconnect: true
      })
      console.log("WORKING Mongo initialized!")
    } catch (err) {
      console.log('WORKING Mongo error: ', err.message)
    }
  }

  async addVectors() {
    // this.vectors = this.vectors || {}
    // this.vectors["SP500_closes"] = await this.downloadClosePrices(".INX")
    // this.vectors["AAPL_closes"] = await this.downloadClosePrices("AAPL")
    // this.vectors["MSFT_closes"] = await this.downloadClosePrices("MSFT")
    // this.vectors["GE_closes"] = await this.downloadClosePrices("GE")

    // this.vectors["SP500_avg20"] = this.calculateAverage(20, this.vectors["SP500_closes"])
    // this.vectors["AAPL_avg20"] = this.calculateAverage(20, this.vectors["AAPL_closes"])
    // this.vectors["MSFT_avg20"] = this.calculateAverage(20, this.vectors["MSFT_closes"])
    // this.vectors["GE_avg20"] = this.calculateAverage(20, this.vectors["GE_closes"])

    // this.vectors["SP500_avg200"] = this.calculateAverage(200, this.vectors["SP500_closes"])
    // this.vectors["AAPL_avg200"] = this.calculateAverage(200, this.vectors["AAPL_closes"])
    // this.vectors["MSFT_avg200"] = this.calculateAverage(200, this.vectors["MSFT_closes"])
    // this.vectors["GE_avg200"] = this.calculateAverage(200, this.vectors["GE_closes"])

    this.vectors["AAPL_closes"] = await this.downloadClosePrices("AAPL")
    this.vectors["AAPL_avg200"] = this.calculateAverage(200, this.vectors["AAPL_closes"])

    this.saveVectors()
  }

  async saveVectors() {
    for (var key in this.vectors) {
      await this.db.collection("vector").save({
        _id: key,
        data: this.vectors[key]
      })
    }
  }

  async loadVectors() {
    this.vectors = {}
    var data = await this.db.collection("vector").find().toArray()
    for (var i = 0; i < data.length; i++) {
      this.vectors[data[i]._id] = data[i].data
    }
  }

  async downloadClosePrices(symbol) {
    var data = await Utils.callService("https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=" + symbol + "&outputsize=full&apikey=6ARG1SKPJ1NHKX6U")
    // var data = await Utils.callService("https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=" + symbol + "&apikey=6ARG1SKPJ1NHKX6U")
    if (!data) return []

    var writeDates = (this.vectors.dates == null)
    if (writeDates) this.vectors.dates = []

    var prices = []
    var keys = Object.keys(data["Time Series (Daily)"]).reverse()
    // for (var i = 0; i < keys.length; i++) {
    for (var i = 0; i < 4528; i++) {
      var date = keys[i]
      if (writeDates) {
        this.vectors.dates.push(date)
      } else {
        if (this.vectors.dates[i] != date) throw Error('invalid date' + date)
      }
      var record = data["Time Series (Daily)"][date]
      prices.push(parseFloat(record["4. close"]))
    }
    // AAPLData.reverse()

    // var prices = []
    // var lastPrice = 0
    // var currDate = new Date(2017, 8, 2) //=2017-09-01
    // var nowDate = new Date()
    // while (currDate <= nowDate) {
    //  var record = data["Time Series (Daily)"][currDate.toISOString().slice(0, 10)]
    //  if (record) {
    //    lastPrice = parseFloat(record["4. close"])
    //  }
    //  prices.push(lastPrice)
    //  currDate.setTime(currDate.getTime() + (24 * 60 * 60 * 1000)) //+1 day
    // }
    return prices
  }

  calculateAverage(d, prices) {
    var avgs = []
    var avg = []
    for (var i = 0; i < prices.length; i++) {
      var p = 0
      if (i < d) {
        p = prices[d] * d
      }
      if (i == d) {
        for (var g = 0; g < d; g++) {
          p += prices[g]
        }
      }
      if (i > d) {
        p = avgs[i - 1]
        p -= prices[i - 1 - d]
        p += prices[i - 1]
      }

      avgs.push(p)
      avg.push(p / d)
    }
    return avg
  }

  async getStatistics() {
    var allTraders = await this.db.collection("trader").count()
    var newTraders = await this.db.collection("trader").find({
      result: null
    }).count()
    return {
      allTraders: allTraders,
      newTraders: newTraders,
      testedTraders: allTraders - newTraders,
      startDate: this.vectors.dates[0],
      endDate: this.vectors.dates[this.vectors.dates.length - 1]
    }
  }

  async getTopTraders() {
    var traders = await this.db.collection("trader").find().sort({
      result: -1
    }).limit(20).toArray()
    return traders
  }

  async getHypotheses() {
    var data = await this.db.collection("hypothesis").find().sort({
      _id: 1
    }).toArray()
    return data
  }

  async setStatus(status) {
    var settings = await this.db.collection("settings").findOne()
    settings.status = status
    await this.db.collection("settings").save(settings)
    return settings
  }

  async getSettings() {
    var settings = await this.db.collection("settings").findOne()
    return settings
  }
}