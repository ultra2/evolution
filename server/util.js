var rp = require('request-promise-native')

export class Utils {

  static getRandomIntInclusive(min, max) {
    min = Math.ceil(min)
    max = Math.floor(max)
    return Math.floor(Math.random() * (max - min + 1)) + min // The maximum is inclusive and the minimum is inclusive
  }

  static async callService(uri) {

    return new Promise((resolve, reject) => {

      var options = {
        uri: uri
      }

      rp(options).then(function(data) {
          resolve(JSON.parse(data))
        })
        .catch(function(err) {
          console.log(err)
          reject(err)
        })

      // options = options || {}
      // options.headers = options.headers || {}
      // options.headers['user-agent'] = 'node.js'
      // request(uri, options, function (err, resp, body) {
      //   if (err) {
      //     reject(err)
      //     return
      //   }
      //   resolve(body)
      // })
    })
  }
}
