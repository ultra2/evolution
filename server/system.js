import {
    Store
}
from './store'
import * as express from 'express'

export class System {

    constructor() {
        this.store = new Store()
        this.app = express.default()
    }

    async run() {
        await this.store.initMongo()
        await this.store.loadVectors()

        this.app.use(express.static('client/dist'))

        this.app.get('/', function (req, res) {
            res.redirect('/index.html')
        })

        this.app.get('/getTraders', async function (req, res) {
            var data = await this.store.getTopTraders()
            res.send(data)
        }.bind(this))

        this.app.get('/getHypotheses', async function (req, res) {
            var data = await this.store.getHypotheses()
            res.send(data)
        }.bind(this))

        this.app.get('/getSettings', async function (req, res) {
            var data = await this.store.getSettings()
            res.send(data)
        }.bind(this))

        this.app.get('/getStatistics', async function (req, res) {
            var data = await this.store.getStatistics()
            res.send(data)
        }.bind(this))

        this.app.post('/start', async function (req, res) {
            var data = await this.store.setStatus(1)
            res.send(data)
        }.bind(this))

        this.app.post('/stop', async function (req, res) {
            var data = await this.store.setStatus(0)
            res.send(data)
        }.bind(this))

        this.app.get('/getVector', async function (req, res) {
            var data = this.store.vectors[req.query["id"]]
            res.send(data)
        }.bind(this))

        this.app.get('/getVectors', async function (req, res) {
            var data = Object.keys(this.store.vectors).map((name) => {
                return {
                    key: name,
                    value: name
                }
            })
            res.send(data)
        }.bind(this))

        this.app.listen(3000, () => console.log('Example app listening on port 3000!'))

        // console.log("Finished")
        // process.exit()
    }
}
