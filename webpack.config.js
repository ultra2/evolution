var path = require('path')
var nodeExternals = require('webpack-node-externals');
var HtmlWebpackPlugin = require('html-webpack-plugin')
module.exports = [{
    watch: false,
    cache: true,
    devtool: 'source-map', 
    entry: './server/index.js',
    target: 'node',
    output: {
        path: __dirname,
        libraryTarget: 'commonjs',
        filename: 'server.js'
    },
    externals: [nodeExternals()],
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        ignore: '/node_modules/',
                        presets: [
                            'es2015', //es2015: converts es6 import to require for nodejs
                        ]
                    }
                }
            }
        ]
    }
}, {
    watch: false,
    cache: true,
    devtool: 'source-map',
    entry: './client/index.js',
    output: {
        path: __dirname + '/client/dist',
        filename: 'client.js'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        ignore: '/node_modules/',
                        presets: [
                            'es2015', //es2015: converts es6 import to require for nodejs
                        ]
                    }
                }
            },
            {
                test: /\.vue$/,
                use: [{
                    loader: 'vue-loader',
                    options: {}
                }, {
                    loader: 'iview-loader',
                    options: {
                        prefix: false
                    }
                }]
            }, {
                test: /\.(png|jpg|gif)$/,
                loader: 'file-loader',
                options: {
                    objectAssign: 'Object.assign'
                }
            }, {
                test: /\.css$/,
                loader: ['style-loader', 'css-loader']
            }, {
                test: /\.(eot|woff|woff2|svg|ttf)$/,
                loader: "file-loader"
            }
        ]
    },
    plugins: [new HtmlWebpackPlugin({
        template: 'client/index.html'
    })]
}]
