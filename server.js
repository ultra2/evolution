(function(e, a) { for(var i in a) e[i] = a[i]; }(exports, /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _system = __webpack_require__(1);

var system = new _system.System();
system.run();

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.System = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _store = __webpack_require__(2);

var _express = __webpack_require__(6);

var express = _interopRequireWildcard(_express);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var System = exports.System = function () {
    function System() {
        _classCallCheck(this, System);

        this.store = new _store.Store();
        this.app = express.default();
    }

    _createClass(System, [{
        key: 'run',
        value: async function run() {
            await this.store.initMongo();
            await this.store.loadVectors();

            this.app.use(express.static('client/dist'));

            this.app.get('/', function (req, res) {
                res.redirect('/index.html');
            });

            this.app.get('/getTraders', async function (req, res) {
                var data = await this.store.getTopTraders();
                res.send(data);
            }.bind(this));

            this.app.get('/getHypotheses', async function (req, res) {
                var data = await this.store.getHypotheses();
                res.send(data);
            }.bind(this));

            this.app.get('/getSettings', async function (req, res) {
                var data = await this.store.getSettings();
                res.send(data);
            }.bind(this));

            this.app.get('/getStatistics', async function (req, res) {
                var data = await this.store.getStatistics();
                res.send(data);
            }.bind(this));

            this.app.post('/start', async function (req, res) {
                var data = await this.store.setStatus(1);
                res.send(data);
            }.bind(this));

            this.app.post('/stop', async function (req, res) {
                var data = await this.store.setStatus(0);
                res.send(data);
            }.bind(this));

            this.app.get('/getVector', async function (req, res) {
                var data = this.store.vectors[req.query["id"]];
                res.send(data);
            }.bind(this));

            this.app.get('/getVectors', async function (req, res) {
                var data = Object.keys(this.store.vectors).map(function (name) {
                    return {
                        key: name,
                        value: name
                    };
                });
                res.send(data);
            }.bind(this));

            this.app.listen(3000, function () {
                return console.log('Example app listening on port 3000!');
            });

            // console.log("Finished")
            // process.exit()
        }
    }]);

    return System;
}();

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Store = undefined;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _mongodb = __webpack_require__(3);

var _util = __webpack_require__(4);

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Store = exports.Store = function () {
  function Store() {
    _classCallCheck(this, Store);

    // this.url = "mongodb://admin:TV123456@ds040637.mlab.com:40637/evolution"
    this.url = 'mongodb://localhost:27017/evolution';
  }

  _createClass(Store, [{
    key: 'initMongo',
    value: async function initMongo() {
      try {
        this.db = await _mongodb.MongoClient.connect(this.url, {
          autoReconnect: true
        });
        console.log("WORKING Mongo initialized!");
      } catch (err) {
        console.log('WORKING Mongo error: ', err.message);
      }
    }
  }, {
    key: 'addVectors',
    value: async function addVectors() {
      // this.vectors = this.vectors || {}
      // this.vectors["SP500_closes"] = await this.downloadClosePrices(".INX")
      // this.vectors["AAPL_closes"] = await this.downloadClosePrices("AAPL")
      // this.vectors["MSFT_closes"] = await this.downloadClosePrices("MSFT")
      // this.vectors["GE_closes"] = await this.downloadClosePrices("GE")

      // this.vectors["SP500_avg20"] = this.calculateAverage(20, this.vectors["SP500_closes"])
      // this.vectors["AAPL_avg20"] = this.calculateAverage(20, this.vectors["AAPL_closes"])
      // this.vectors["MSFT_avg20"] = this.calculateAverage(20, this.vectors["MSFT_closes"])
      // this.vectors["GE_avg20"] = this.calculateAverage(20, this.vectors["GE_closes"])

      // this.vectors["SP500_avg200"] = this.calculateAverage(200, this.vectors["SP500_closes"])
      // this.vectors["AAPL_avg200"] = this.calculateAverage(200, this.vectors["AAPL_closes"])
      // this.vectors["MSFT_avg200"] = this.calculateAverage(200, this.vectors["MSFT_closes"])
      // this.vectors["GE_avg200"] = this.calculateAverage(200, this.vectors["GE_closes"])

      this.vectors["AAPL_closes"] = await this.downloadClosePrices("AAPL");
      this.vectors["AAPL_avg200"] = this.calculateAverage(200, this.vectors["AAPL_closes"]);

      this.saveVectors();
    }
  }, {
    key: 'saveVectors',
    value: async function saveVectors() {
      for (var key in this.vectors) {
        await this.db.collection("vector").save({
          _id: key,
          data: this.vectors[key]
        });
      }
    }
  }, {
    key: 'loadVectors',
    value: async function loadVectors() {
      this.vectors = {};
      var data = await this.db.collection("vector").find().toArray();
      for (var i = 0; i < data.length; i++) {
        this.vectors[data[i]._id] = data[i].data;
      }
    }
  }, {
    key: 'downloadClosePrices',
    value: async function downloadClosePrices(symbol) {
      var data = await _util.Utils.callService("https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=" + symbol + "&outputsize=full&apikey=6ARG1SKPJ1NHKX6U");
      // var data = await Utils.callService("https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=" + symbol + "&apikey=6ARG1SKPJ1NHKX6U")
      if (!data) return [];

      var writeDates = this.vectors.dates == null;
      if (writeDates) this.vectors.dates = [];

      var prices = [];
      var keys = Object.keys(data["Time Series (Daily)"]).reverse();
      // for (var i = 0; i < keys.length; i++) {
      for (var i = 0; i < 4528; i++) {
        var date = keys[i];
        if (writeDates) {
          this.vectors.dates.push(date);
        } else {
          if (this.vectors.dates[i] != date) throw Error('invalid date' + date);
        }
        var record = data["Time Series (Daily)"][date];
        prices.push(parseFloat(record["4. close"]));
      }
      // AAPLData.reverse()

      // var prices = []
      // var lastPrice = 0
      // var currDate = new Date(2017, 8, 2) //=2017-09-01
      // var nowDate = new Date()
      // while (currDate <= nowDate) {
      //  var record = data["Time Series (Daily)"][currDate.toISOString().slice(0, 10)]
      //  if (record) {
      //    lastPrice = parseFloat(record["4. close"])
      //  }
      //  prices.push(lastPrice)
      //  currDate.setTime(currDate.getTime() + (24 * 60 * 60 * 1000)) //+1 day
      // }
      return prices;
    }
  }, {
    key: 'calculateAverage',
    value: function calculateAverage(d, prices) {
      var avgs = [];
      var avg = [];
      for (var i = 0; i < prices.length; i++) {
        var p = 0;
        if (i < d) {
          p = prices[d] * d;
        }
        if (i == d) {
          for (var g = 0; g < d; g++) {
            p += prices[g];
          }
        }
        if (i > d) {
          p = avgs[i - 1];
          p -= prices[i - 1 - d];
          p += prices[i - 1];
        }

        avgs.push(p);
        avg.push(p / d);
      }
      return avg;
    }
  }, {
    key: 'getStatistics',
    value: async function getStatistics() {
      var allTraders = await this.db.collection("trader").count();
      var newTraders = await this.db.collection("trader").find({
        result: null
      }).count();
      return {
        allTraders: allTraders,
        newTraders: newTraders,
        testedTraders: allTraders - newTraders,
        startDate: this.vectors.dates[0],
        endDate: this.vectors.dates[this.vectors.dates.length - 1]
      };
    }
  }, {
    key: 'getTopTraders',
    value: async function getTopTraders() {
      var traders = await this.db.collection("trader").find().sort({
        result: -1
      }).limit(20).toArray();
      return traders;
    }
  }, {
    key: 'getHypotheses',
    value: async function getHypotheses() {
      var data = await this.db.collection("hypothesis").find().sort({
        _id: 1
      }).toArray();
      return data;
    }
  }, {
    key: 'setStatus',
    value: async function setStatus(status) {
      var settings = await this.db.collection("settings").findOne();
      settings.status = status;
      await this.db.collection("settings").save(settings);
      return settings;
    }
  }, {
    key: 'getSettings',
    value: async function getSettings() {
      var settings = await this.db.collection("settings").findOne();
      return settings;
    }
  }]);

  return Store;
}();

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = require("mongodb");

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var rp = __webpack_require__(5);

var Utils = exports.Utils = function () {
  function Utils() {
    _classCallCheck(this, Utils);
  }

  _createClass(Utils, null, [{
    key: 'getRandomIntInclusive',
    value: function getRandomIntInclusive(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1)) + min; // The maximum is inclusive and the minimum is inclusive
    }
  }, {
    key: 'callService',
    value: async function callService(uri) {

      return new Promise(function (resolve, reject) {

        var options = {
          uri: uri
        };

        rp(options).then(function (data) {
          resolve(JSON.parse(data));
        }).catch(function (err) {
          console.log(err);
          reject(err);
        });

        // options = options || {}
        // options.headers = options.headers || {}
        // options.headers['user-agent'] = 'node.js'
        // request(uri, options, function (err, resp, body) {
        //   if (err) {
        //     reject(err)
        //     return
        //   }
        //   resolve(body)
        // })
      });
    }
  }]);

  return Utils;
}();

/***/ }),
/* 5 */
/***/ (function(module, exports) {

module.exports = require("request-promise-native");

/***/ }),
/* 6 */
/***/ (function(module, exports) {

module.exports = require("express");

/***/ })
/******/ ])));
//# sourceMappingURL=server.js.map