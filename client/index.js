import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './app.vue'
import store from './store'
import http from './http'

// import Vuetify from 'vuetify'
// import styles from 'vuetify/dist/vuetify.css'
import iView from 'iview'
import 'iview/dist/styles/iview.css'

// import VueChart from 'vue-chart-js'
import VueChart from './components/chart.vue'
// import ActivityGraph from './components/ActivityGraph/ActivityGraph.vue'
import routes from './routes'
// import Util from './libs/util'

// 1. Use plugin.
// This installs <router-view> and <router-link>,
// and injects $router and $route to all router-enabled child components
Vue.use(VueRouter)

// Vue.use(VueChart)

// Vue.use(Vuetify)
Vue.use(iView)

// Vue.component("activity-graph", ActivityGraph)
Vue.component("vue-chart2", VueChart)

// 3. Create the router
const RouterConfig = {
    routes: routes
}
const router = new VueRouter(RouterConfig)

// router.beforeEach((to, from, next) => {
//   iView.LoadingBar.start();
//   // Util.title(to.meta.title);
//   next();
// })

// router.afterEach((to, from, next) => {
//   iView.LoadingBar.finish();
//   window.scrollTo(0, 0);
// })

var vm = new Vue({
    el: '#app',
    store: store,
    router: router,
    render: h => h(App)
})
