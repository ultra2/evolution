webpackJsonp([3],{

/***/ 212:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_bustCache_node_modules_iview_loader_index_js_ref_1_1_vectors_vue__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_770db8b9_hasScoped_false_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_bustCache_node_modules_iview_loader_index_js_ref_1_1_vectors_vue__ = __webpack_require__(214);
var disposed = false
var normalizeComponent = __webpack_require__(9)
/* script */

/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_bustCache_node_modules_iview_loader_index_js_ref_1_1_vectors_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_770db8b9_hasScoped_false_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_bustCache_node_modules_iview_loader_index_js_ref_1_1_vectors_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "client/views/vectors.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {  return key !== "default" && key.substr(0, 2) !== "__"})) {  console.error("named exports are not supported in *.vue files.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-770db8b9", Component.options)
  } else {
    hotAPI.reload("data-v-770db8b9", Component.options)
' + '  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),

/***/ 213:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["a"] = ({
  data() {
    return {
      vector: null,
      vectors: [],
      vectorchart: null,
      vectorData: {
        labels: [],
        datasets: []
      },
      vectorOptions: {
        fill: false,
        responsive: true,
        scales: {
          xAxes: [{}]
        }
      }
    };
  },
  methods: {
    refreshDates() {
      this.$http.get('/getVector?id=dates').then(response => {
        this.vectorData.labels = response.body;
      });
    },
    refreshVectors() {
      this.$http.get('/getVectors').then(response => {
        this.vectors = response.body;
      });
    },
    addVector() {
      this.$http.get('/getVector?id=' + this.vector).then(response => {
        var colors = ["red", "green", "blue", 'purple', 'yellow'];
        var color = colors[this.vectorData.datasets.length];
        this.vectorData.datasets.push({
          label: this.vector,
          borderColor: color,
          data: response.body
        });
      });
    },
    clearVectors() {
      this.vectorData.datasets = [];
    }
  },
  mounted: function () {
    this.refreshVectors();
    this.refreshDates();
  }
});

/***/ }),

/***/ 214:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "Form",
        { attrs: { model: _vm.formItem, "label-width": 80 } },
        [
          _c(
            "FormItem",
            { attrs: { label: "Vector" } },
            [
              _c(
                "Select",
                {
                  staticStyle: { width: "200px" },
                  attrs: { placeholder: "" },
                  model: {
                    value: _vm.vector,
                    callback: function($$v) {
                      _vm.vector = $$v
                    },
                    expression: "vector"
                  }
                },
                _vm._l(_vm.vectors, function(item) {
                  return _c(
                    "Option",
                    { key: item.key, attrs: { value: item.value } },
                    [_vm._v(_vm._s(item.value))]
                  )
                })
              ),
              _vm._v(" "),
              _c(
                "Button",
                { attrs: { type: "success" }, on: { click: _vm.addVector } },
                [_vm._v("Add")]
              ),
              _vm._v(" "),
              _c(
                "Button",
                { attrs: { type: "error" }, on: { click: _vm.clearVectors } },
                [_vm._v("Clear")]
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("vue-chart2", {
        ref: "vectorchart",
        attrs: {
          type: "line",
          data: _vm.vectorData,
          options: _vm.vectorOptions
        }
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-770db8b9", esExports)
  }
}

/***/ })

});
//# sourceMappingURL=3.client.js.map