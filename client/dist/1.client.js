webpackJsonp([1],{

/***/ 233:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_bustCache_node_modules_iview_loader_index_js_ref_1_1_traders_vue__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_0535d564_hasScoped_false_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_bustCache_node_modules_iview_loader_index_js_ref_1_1_traders_vue__ = __webpack_require__(236);
var disposed = false
var normalizeComponent = __webpack_require__(11)
/* script */

/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_bustCache_node_modules_iview_loader_index_js_ref_1_1_traders_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_0535d564_hasScoped_false_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_bustCache_node_modules_iview_loader_index_js_ref_1_1_traders_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "client/views/traders.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {  return key !== "default" && key.substr(0, 2) !== "__"})) {  console.error("named exports are not supported in *.vue files.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-0535d564", Component.options)
  } else {
    hotAPI.reload("data-v-0535d564", Component.options)
' + '  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),

/***/ 235:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["a"] = ({
    data() {
        return {
            autoRefreshTraders: false,
            autoRefreshTimer: null,
            tradersData: {
                labels: [],
                datasets: []
            },
            tradersOptions: {
                title: {
                    display: true,
                    text: 'Top 20 traders',
                    fontSize: 14
                },
                scales: {
                    yAxes: [{
                        stacked: false
                        //scaleLabel: {
                        //  display: true,
                        //  labelString: "traders"
                        //}
                    }]
                }
            },
            hypotheses: []
        };
    },
    computed: {
        statistics() {
            return this.$store.state.statistics;
        }
    },
    watch: {
        autoRefreshTraders: function (val) {
            if (val) {
                this.autoRefreshTimer = setInterval(this.refreshTimer, 1000);
            } else {
                clearInterval(this.autoRefreshTimer);
            }
        }
    },
    methods: {
        refreshTimer() {
            this.refreshTraders();
            this.refreshStatistics();
        },
        async refreshStatistics() {
            const response = await this.$http.get('/getStatistics');
            this.$store.state.statistics = response.data;
        },
        async refreshTraders() {
            const response = await this.$http.get('/getTraders');
            var labels = [];
            var datasets = [];
            var labels = [];
            for (var trader of response.data) {
                //labels.push(trader._id)
                //var a = ", " + trader.dns[dns0] + "/" + trader.dns[dns1]
                labels.push(Math.round(trader.result));
            }
            this.tradersData.labels = labels;

            var datasets = [];
            for (var h of this.hypotheses) {
                var dataset = {
                    label: h._id,
                    backgroundColor: h.color,
                    data: []
                };

                for (var trader of response.data) {
                    var weight = trader.dns[h._id] || 0;
                    //var v = trader.result * (Math.abs(weight) / 10)
                    //var v = trader.result * (weight / 10)
                    var v = weight;
                    dataset.data.push(v);
                }

                datasets.push(dataset);
            }
            this.tradersData.datasets = datasets;
        },
        async refreshHypotheses() {
            const response = await this.$http.get('/getHypotheses');
            this.hypotheses = response.data;
        }
    },
    mounted: function () {
        this.refreshStatistics();
        this.refreshTraders();
        this.refreshHypotheses();
    },
    beforeDestroy: function () {
        clearInterval(this.autoRefreshTimer);
    }
});

/***/ }),

/***/ 236:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c(
        "Row",
        [
          _c(
            "Button",
            {
              attrs: { type: "primary", icon: "refresh" },
              on: { click: _vm.refreshTraders }
            },
            [_vm._v("Refresh")]
          ),
          _vm._v(" "),
          _c(
            "i-switch",
            {
              attrs: { size: "large" },
              model: {
                value: _vm.autoRefreshTraders,
                callback: function($$v) {
                  _vm.autoRefreshTraders = $$v
                },
                expression: "autoRefreshTraders"
              }
            },
            [
              _c("span", { attrs: { slot: "open" }, slot: "open" }, [
                _vm._v("ON")
              ]),
              _vm._v(" "),
              _c("span", { attrs: { slot: "close" }, slot: "close" }, [
                _vm._v("OFF")
              ])
            ]
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "Row",
        [
          _vm.statistics
            ? _c("p", {
                domProps: {
                  textContent: _vm._s(
                    "Traders: " +
                      _vm.statistics.allTraders +
                      " (" +
                      _vm.statistics.testedTraders +
                      ")"
                  )
                }
              })
            : _vm._e(),
          _vm._v(" "),
          _vm.statistics
            ? _c("p", {
                domProps: {
                  textContent: _vm._s(
                    "Duration: " +
                      _vm.statistics.startDate +
                      " - " +
                      _vm.statistics.endDate
                  )
                }
              })
            : _vm._e(),
          _vm._v(" "),
          _c("vue-chart2", {
            attrs: {
              type: "horizontalBar",
              data: _vm.tradersData,
              options: _vm.tradersOptions
            }
          })
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-0535d564", esExports)
  }
}

/***/ })

});
//# sourceMappingURL=1.client.js.map