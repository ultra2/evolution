webpackJsonp([2],{

/***/ 207:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_bustCache_node_modules_iview_loader_index_js_ref_1_1_index_vue__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_46b2993b_hasScoped_false_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_bustCache_node_modules_iview_loader_index_js_ref_1_1_index_vue__ = __webpack_require__(218);
var disposed = false
var normalizeComponent = __webpack_require__(9)
/* script */

/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_bustCache_node_modules_iview_loader_index_js_ref_1_1_index_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_46b2993b_hasScoped_false_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_bustCache_node_modules_iview_loader_index_js_ref_1_1_index_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "client/views/index.vue"
if (Component.esModule && Object.keys(Component.esModule).some(function (key) {  return key !== "default" && key.substr(0, 2) !== "__"})) {  console.error("named exports are not supported in *.vue files.")}

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-46b2993b", Component.options)
  } else {
    hotAPI.reload("data-v-46b2993b", Component.options)
' + '  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

/* harmony default export */ __webpack_exports__["default"] = (Component.exports);


/***/ }),

/***/ 210:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["a"] = ({
  data() {
    return {
      statistics: null,
      tradersData: {
        labels: [],
        datasets: []
      },
      tradersOptions: {
        title: {
          display: true,
          text: 'Top 20 traders',
          fontSize: 14
        },
        scales: {
          yAxes: [{
            stacked: false
            //scaleLabel: {
            //  display: true,
            //  labelString: "traders"
            //}
          }]
        }
      },
      hypotheses: []
    };
  },
  methods: {

    refreshTraders() {
      this.$http.get('/getTraders').then(response => {
        var labels = [];
        var datasets = [];
        // var datasets = [{
        //   label: '',
        //   backgroundColor: 'red',
        //   data: []
        // }, {
        //   label: '',
        //   backgroundColor: 'blue',
        //   data: []
        // }]

        var labels = [];
        for (var trader of response.body) {
          //labels.push(trader._id)
          //var a = ", " + trader.dns[dns0] + "/" + trader.dns[dns1]
          labels.push(Math.round(trader.result));
        }
        this.tradersData.labels = labels;

        var datasets = [];
        for (var h of this.hypotheses) {
          var dataset = {
            label: h._id,
            backgroundColor: h.color,
            data: []
          };

          for (var trader of response.body) {
            var weight = trader.dns[h._id] || 0;
            //var v = trader.result * (Math.abs(weight) / 10)
            //var v = trader.result * (weight / 10)
            var v = weight;
            dataset.data.push(v);
          }

          datasets.push(dataset);
        }
        this.tradersData.datasets = datasets;
      }, response => {
        alert(response);
      });
    },
    refreshStatistics() {
      this.$http.get('/getStatistics').then(response => {
        this.statistics = response.body;
      }, response => {
        alert(response);
      });
    },
    refreshHypotheses() {
      this.$http.get('/getHypotheses').then(response => {
        this.hypotheses = response.body;
      });
    }
  },
  mounted: function () {
    this.refreshStatistics();
    this.refreshHypotheses();
    this.refreshTraders();
  }
});

/***/ }),

/***/ 218:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "div",
      { staticStyle: { "min-height": "200px" } },
      [
        _vm.statistics
          ? _c("p", {
              domProps: {
                textContent: _vm._s(
                  "Traders: " +
                    _vm.statistics.allTraders +
                    " (" +
                    _vm.statistics.testedTraders +
                    ")"
                )
              }
            })
          : _vm._e(),
        _vm._v(" "),
        _vm.statistics
          ? _c("p", {
              domProps: {
                textContent: _vm._s(
                  "Duration: " +
                    _vm.statistics.startDate +
                    " - " +
                    _vm.statistics.endDate
                )
              }
            })
          : _vm._e(),
        _vm._v(" "),
        _c("vue-chart2", {
          attrs: {
            type: "horizontalBar",
            data: _vm.tradersData,
            options: _vm.tradersOptions
          }
        })
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-46b2993b", esExports)
  }
}

/***/ })

});
//# sourceMappingURL=2.client.js.map