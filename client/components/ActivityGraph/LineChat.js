import {
  Line
} from 'vue-chartjs'

export default Line.extend({
  mounted() {
    this.rows = ["aa", "bb"]
    this.labels = ["cc", "dd"]
  },
  data() {
    return {
      rows: [],
      labels: []
    }
  },
  methods: {
    setUpGraph() {
      this.renderChart({
        labels: this.labels,
        datasets: [{
          label: 'My activities',
          backgroundColor: '#dd4b39',
          data: this.rows
        }]
      }, {
        responsive: true,
        maintainAspectRatio: false
      })
    }
  }
})