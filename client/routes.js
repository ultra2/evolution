// const traders = () =>
//    import ('./views/traders.vue')
// const vectors = () =>
//    import ('./views/vectors.vue')

import traders from './views/traders.vue'
import vectors from './views/vectors.vue'

const routes = [{
    path: '/',
    redirect: '/traders'
  }, {
    path: '/traders',
    meta: {
        title: ''
    },
    component: traders
  }, {
    path: '/vectors',
    meta: {
        title: ''
    },
    component: vectors
  }]
export default routes
