import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'

// Example:
// var defaultaxios = axios.create({
//   headers: {
//     Authorization: 'Bearer {token}'
//   }
// })
// Vue.use(VueAxios, defaultaxios)

function aaa(a) {
  alert(a)
}

Vue.use(VueAxios, axios)
