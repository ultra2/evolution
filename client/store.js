import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        statistics: {
            allTraders: 10,
            testedTraders: 5
        },
        settings: null
    },
    actions: {}
})

export default store
