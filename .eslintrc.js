module.exports = {
    "extends": [
        // add more generic rulesets here, such as:
        'eslint:recommended',
        'plugin:vue/essential'
    ],
    "parserOptions": {
        "ecmaVersion": 2017,
        "sourceType": "module"
    },
    "env": {
        "es6": true,
        "node": true,
        "browser": true
    },
    "rules": {
        "space-before-function-paren": 0,
        "indent": 0,
        "padded-blocks": 0,
        "quotes": 0,
        "eqeqeq": 0,
        "no-unused-vars": 0,
        "no-console": 0
    }
}